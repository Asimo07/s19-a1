const cube = 2 ** 3;

console.log(`The cube of 2 is ${cube}`);

const address = [258, "Washington Ave NW,", "California", 90011];

const [number, name, state, zipCode] = address;

console.log(`I live at ${number} ${name} ${state} ${zipCode}`);

const animal= {
	animalName: "Lolong",
	animalType: ['saltwater crocodile.'],
	weight: 1075,
	height: ["20 ft 3 in."]
}

const {animalName, animalType, weight, height} = animalDetail;

function animalDetail({animalName, animalType, weight, height}){
	console.log(`${animalName} was a ${animalType} He weighed at ${weight} kgs with a measurement of ${height} `);
}
animalDetail(animal);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, ['Miniature Dachshund']);

console.log(myDog);